pub mod javier_novoa;

use cv::Profesional;
use javier_novoa::Profile;

fn main() {
    let jnc = Profile {
        name: String::from("M. en C. Javier Novoa Cataño"),
        email: String::from("jstitch@gmail.com"),
        web: String::from("https://gitlab.com/jstitch/cv-business-card"),
        location: String::from("Atizapan, edo Mex, Mexico"),
    };

    println!("Hello! I am: {}\n", jnc);
    println!("My skills: {:?}\n", jnc.skills());
    println!("I master: {:?}\n", jnc.languages());
    println!("I am capable of: {:?}\n", jnc.soft());
    println!("Hobbies: 👨‍👩‍👧‍👦 🎸📖🔭✒️ ♟️ 🖥️");
}

// rust 1.66.1
// emacs 28.2
// lsp-mode 20230221.1558
// gimp 2.10.32
