use cv::tech::{Cloud, Languages, Skills, SoftSkills};
use cv::Profesional;

pub struct Profile {
    pub name: String,
    pub email: String,
    pub web: String,
    pub location: String,
}

impl Profesional<Languages, SoftSkills, Skills> for Profile {
    fn languages(&self) -> Vec<Languages> {
        vec![Languages::Python, Languages::Rust, Languages::CppC]
    }
    fn skills(&self) -> Vec<Skills> {
        vec![
            Skills::BackendDevSr,
            Skills::DesignPatterns,
            Skills::Git,
            Skills::Cloud(Cloud::AWS),
            Skills::DDD,
            Skills::Linux,
        ]
    }
    fn soft(&self) -> Vec<SoftSkills> {
        vec![
            SoftSkills::DevOpsCulture,
            SoftSkills::TechLead,
            SoftSkills::TechCoach,
            SoftSkills::Agile,
        ]
    }
}

use std::fmt;
impl fmt::Display for Profile {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}, {}\n{}\n{}",
            self.name, self.email, self.location, self.web
        )
    }
}
