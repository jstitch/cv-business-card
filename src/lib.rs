pub trait Profesional<T, U, V> {
    fn languages(&self) -> Vec<T>;
    fn soft(&self) -> Vec<U>;
    fn skills(&self) -> Vec<V>;
}

pub mod tech {
    #[derive(Debug)]
    pub enum Cloud {
        AWS,
        Azure,
    }
    #[derive(Debug)]
    pub enum Languages {
        Python,
        CppC,
        Java,
        Rust,
    }
    #[derive(Debug)]
    pub enum SoftSkills {
        TechLead,
        TechCoach,
        DevOpsCulture,
        Agile,
    }
    #[derive(Debug)]
    pub enum Skills {
        BackendDevSr,
        DesignPatterns,
        DDD,
        Cloud(Cloud),
        Linux,
        Git,
    }
}
